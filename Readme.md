# Primera Reunión Latinoamericana de usuarios QGIS 2020

QGIS LATAM es una reunión virtual de usuarios de Latinoamérica para compartir sus experiencias ene luso de tecnologías opersource geoespaciales y el uso de QGIS en el ámbito gubernamental, educativo, público, social y privado en temas como: Recursos naturales, Urbanismo, Espacio, sociedad y cultura, SIG y educación, Percepción Remota, Datos Geográficos y ambientes colaborativos, software libre geoespacial, uso de estándares, Geomática, entre otros.

## Programa

Celebrado del 23-27 de Noviembre del 2020, más detalles en el micrositio del evento [QGIS LATAM](https://qgis.mx/latam/).

## Lunes 23
```bash
M1 - Bienvenida QGIS LATAM
M2 - Esteban Zimányi (MobilityDB)
G1 - QGIS Argentina
P01 - Relación entre índices espectrales y la productividad forrajera en un región árida y semiárida de la Patagonia Argentina Buzzi, Mariana; Roque, Luis Fernando
P02 - NatureMap: Un algoritmo con programación lineal para optimizar criterios de conservación ecológica a través de la integración entre R y QGIS - Camilo Alcántara
T05 - Geopackage y Mapas Leaflet Lourdes Hermosillo
```
## Martes 24
```bash
T01 - OpenDroneMap - Comunidad OpenDroneMap MX
M3 - Vicky Vergara Board of Director OSGEO
M4 - Mauricio Márquez Consultor
G2 - QGIS Brasil
G3 - QGIS Chile
T06 - Nubes puntos en QGIS - Hennessy Becerra
```
## Miércoles 25
```bash
T02 - PGRouting - Vicky Vergara
M5 - José Luis Mondragón Garibay INEGI México
G4 - QGIS Colombia
P03 - Q-GIS y SIG-P aplicados en el aula indígena: revaloración del conocimiento tradicional y defensa del territorio - Rocío Martínez González
P04 - Desarrollo de plugins para la automatización de procesos geográficos en la CONABIO - Díaz Bernal Roberto; Roque Vilchis Luis Fernando; Dávila Rosas José Manuel.
P05 - Agricultura de precisión a partir de fusión de imágenes satelitales por métodos convencionales y transformadas Wavelet Daubenchies y Coiflet - Cortes Millan Yosef Harvey
P06 - Manipulación de datos LiDAR y de fotogrametría digital en arqueología - Javier López Mejía, Gerardo Jiménez Delgado, Berenice Jiménez González and Guillermo Acosta Ochoa
T07 - QGIS Básico - Alejandro Lepe
```
## Jueves 26
```bash
T03 - Estimación de temperatura superficial con imágenes satélitales - Marcela chavoya
M6 - Hans Van Der Kwast, IHE Delft Institute for Water Education
G5 - QGIS Costa Rica
G6 - QGIS México
P07 - Procesamiento y visualización de datos UAV en QGIS con implementación con BIM (Building Information Managment) y realidad virtual - Ana Karen León Miranda
P08 - Evaluación de parámetros de segmentación en OBIA para la clasificación de coberturas del suelo a partir de imágenes VANT - Susana Isabel Hinojosa Espinoza
T08 - PostGIS Básico - PostGIS México
```
## Viernes 27
```bash
T04 - Animación de huracanes en QGIS - Verónica Totolhua
M7 - R con QGIS - Gabo Gaona
P09 - QGIS para tratamiento y servidor imágenes de los satélites GOES-R - M.C. Jorge Humberto Bravo Méndez, Dr. Saúl Miranda Alonso
P10 - QGIS un software libre geoespacial en constante crecimiento que contribuye al desarrollo científico: un análisis bibliométrico
JL Gallardo
G7 - QGIS Perú
P11 - Historias COVID Costa Rica - Olman Fuentes
```

## QGIS LATAM 2020
[QGIS LATAM](https://qgis.mx/latam/)
[Twitter](https://twitter.com/qgislatam)